import { TestBed } from '@angular/core/testing';

import { LoadingPantallaService } from './loading-pantalla.service';

describe('LoadingPantallaService', () => {
  let service: LoadingPantallaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoadingPantallaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
