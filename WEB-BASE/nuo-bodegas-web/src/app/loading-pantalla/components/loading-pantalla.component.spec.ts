import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingPantallaComponent } from './loading-pantalla.component';

describe('LoadingPantallaComponent', () => {
  let component: LoadingPantallaComponent;
  let fixture: ComponentFixture<LoadingPantallaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingPantallaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingPantallaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
