import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit  } from '@angular/core';
import { LoadingPantallaService } from '../services/loading-pantalla.service';
import { Subscription } from 'rxjs';
import { debounceTime } from "rxjs/operators";

@Component({
  selector: 'app-loading-pantalla',
  templateUrl: './loading-pantalla.component.html',
  styleUrls: ['./loading-pantalla.component.scss']
})
export class LoadingPantallaComponent implements OnInit, OnDestroy {
  debounceTime: number = 200;
  loading: boolean = false;
  loadingSubscription: Subscription;

  constructor(private loadingPantallaService: LoadingPantallaService,private _elmRef: ElementRef,
    private _changeDetectorRef: ChangeDetectorRef ) {
  }

  ngOnInit() {
    this.loadingSubscription = this.loadingPantallaService.loadingStatus.pipe(
      debounceTime(200)
    ).subscribe((value) => {
      this.loading = value;
    });
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }

}
