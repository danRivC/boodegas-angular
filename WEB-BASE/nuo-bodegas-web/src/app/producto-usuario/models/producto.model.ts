import { Modelos } from '../../core/models/modelos/modelos.model';
import { Proveedor } from '../../core/models/proveedor/proveedor.model';
import { TipoProducto } from '../../core/models/tipo-producto/tipo-producto.model';
import { IBodega } from '../../bodega/models/bodega.model';
import { Ubicacion } from '../../core/models/ubicacion/ubicacion.model';

export class Producto {
    codigo_pr:number;
    nombre_pr:string;
    numero_parte_pr:string;
    numero_serie_pr:string
    estado_pr: boolean;
    modelo:Modelos;
    proveedor: Proveedor;
    tipo: TipoProducto;
}
