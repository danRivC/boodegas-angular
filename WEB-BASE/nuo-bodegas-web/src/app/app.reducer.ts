import { Token } from '@angular/compiler/src/ml_parser/lexer';
import { IUsuario } from './core/models/usuario/usuario.model';

export interface AppState{
    usuario: IUsuario,
    token:Token
}