import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { StockMinimoComponent } from './components/stock-minimo/stock-minimo.component';
import { StockMinimoDetalleComponent } from './components/stock-minimo-detalle/stock-minimo-detalle.component';


const routes: Routes=[
  {path: '', component:StockMinimoComponent},
  {path: 'detalle', component:StockMinimoDetalleComponent}
]
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],exports:[
    RouterModule
  ]
})
export class StockMinimoRoutingModule { }
