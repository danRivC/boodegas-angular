import { Component, OnInit, ViewChild } from '@angular/core';
import { BodegasService } from 'src/app/core/services/bodega/bodegas.service';
import { Router } from '@angular/router';
import { LoadingPantallaService } from 'src/app/loading-pantalla/services/loading-pantalla.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-stock-minimo',
  templateUrl: './stock-minimo.component.html',
  styleUrls: ['./stock-minimo.component.scss']
})
export class StockMinimoComponent implements OnInit {

  titulo='Lista de Bodegas'
  constructor(private bodega: BodegasService, private router:Router, private loadingPantallaService: LoadingPantallaService) { }
  dataSource = new MatTableDataSource<IBodegaDataSource>();
  @ViewChild(MatPaginator, {static:true}) paginador: MatPaginator;
  displayColumns: string[] = ['nombre', 'parte', 'bodega', 'cantidad', 'actions'];
  productos : Array<IBodegaDataSource>;

  ngOnInit() {
    this.loadingPantallaService.startLoading();
    this.bodega.devuelveBodegasPorCiudad(parseInt(localStorage.getItem("usuario_ciudad"))).subscribe(datos=>this.asignarProductos(datos), error=>console.log(error));
    this.dataSource.paginator = this.paginador;
    this.loadingPantallaService.stopLoading();
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  asignarProductos(datos){
    let producto:IBodegaDataSource;
    this.productos = []
    datos.resultado.forEach(element => {
      producto= new IBodegaDataSource();
      producto.codigo = element.codigo;
      producto.nombre = element.nombre;
      producto.ciudad = element.ciudad;
      producto.estado = element.estado;
      this.productos.push(producto);
    });
    this.dataSource.data = this.productos;
  }

  guardarCiudad(producto){
    this.loadingPantallaService.startLoading();
    sessionStorage.setItem('codigoBodega', producto.codigo);
    this.loadingPantallaService.stopLoading();
    this.router.navigate(['stock-minimo/detalle'])
    
  }

}
class IBodegaDataSource{
  public codigo:number;
  public ciudad:string;
  public nombre: string;
  public estado: string;
}
