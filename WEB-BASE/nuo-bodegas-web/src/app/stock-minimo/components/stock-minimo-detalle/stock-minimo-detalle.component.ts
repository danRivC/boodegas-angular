import { Component, OnInit, ViewChild } from '@angular/core';
import { StockService } from 'src/app/core/services/stock/stock.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ExporterService } from 'src/app/core/exporter.service';
import { LoadingPantallaService } from 'src/app/loading-pantalla/services/loading-pantalla.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormGroup } from '@angular/forms';
import { StockMinimoService } from 'src/app/core/services/stock-minimo/stock-minimo.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-stock-minimo-detalle',
  templateUrl: './stock-minimo-detalle.component.html',
  styleUrls: ['./stock-minimo-detalle.component.scss']
})
export class StockMinimoDetalleComponent implements OnInit {

  titulo = 'Stock Mínimo'
  constructor(private stockService: StockMinimoService ,private snackBar:MatSnackBar,private location: Location, 
    private exporterService:ExporterService, private loadingPantallaService:LoadingPantallaService) { }
  dataSource = new MatTableDataSource<IStock>();
  @ViewChild(MatPaginator, {static:true}) paginador: MatPaginator;
  displayColumns: string[] = ['parte', 'nombre', 'serie','cantidad',  'modelo', 'ubicacion'];
  productos : Array<IStock>;
  busquedaForm : FormGroup;
  
  
  ngOnInit() {
    
    let codigo_bodega = parseInt(sessionStorage.getItem('codigoBodega'))
    console.log('BODEGA:', codigo_bodega)
   this.stockService.obtenerDetalle(codigo_bodega).subscribe(datos=>this.asignarProductos(datos), error=>console.log(error))
   this.dataSource.paginator = this.paginador;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  

  asignarProductos(datos){
    console.log(datos)
    let producto:IStock;
    this.productos = []
    datos.resultado.forEach(element => {
       producto= new IStock();
  
      producto.nombre = element.nombre;
      producto.serie = element.serie;
      producto.parte = element.parte;
      producto.cantidad = element.cantidad;
    
      producto.modelo = element.modelo;
      producto.ubicacion = element.ubicacion;

      this.productos.push(producto);
    });
    this.dataSource.data = this.productos;
  }
  regresar(){
    this.location.back();
    sessionStorage.clear();
  }

  exporterAsExcel(){
   
    this.exporterService.exportToExcel(this.dataSource.filteredData, 'stock_reporte');
    
    
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
class IStock{
  parte: string;
  nombre: string;
  serie : string;
  cantidad :number;
  modelo:string;
  ubicacion :string;
}

