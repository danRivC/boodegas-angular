import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockMinimoDetalleComponent } from './stock-minimo-detalle.component';

describe('StockMinimoDetalleComponent', () => {
  let component: StockMinimoDetalleComponent;
  let fixture: ComponentFixture<StockMinimoDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockMinimoDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockMinimoDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
