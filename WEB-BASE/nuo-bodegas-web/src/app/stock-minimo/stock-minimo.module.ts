import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockMinimoDetalleComponent } from './components/stock-minimo-detalle/stock-minimo-detalle.component';
import { StockMinimoComponent } from './components/stock-minimo/stock-minimo.component';
import { StockMinimoRoutingModule } from './stock-minimo-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../material/material.module';



@NgModule({
  declarations: [
    StockMinimoDetalleComponent,
    StockMinimoComponent,
  ]
    ,
  imports: [
    CommonModule,
    StockMinimoRoutingModule,
    SharedModule,
    MaterialModule
  ]
})
export class StockMinimoModule { }
