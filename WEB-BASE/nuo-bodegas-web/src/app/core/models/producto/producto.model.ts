import { Modelos } from '../modelos/modelos.model';
import { Proveedor } from '../proveedor/proveedor.model';
import { TipoProducto } from '../tipo-producto/tipo-producto.model';
import { IBodega } from '../../../bodega/models/bodega.model';
import { Ubicacion } from '../ubicacion/ubicacion.model';

export class Producto {
    codigo_pr:number;
    nombre_pr:string;
    numero_parte_pr:string;
    numero_serie_pr:string
    estado_pr: boolean;
    modelo:Modelos;
    proveedor: Proveedor;
    tipo: TipoProducto;
    minimo: number;
}
