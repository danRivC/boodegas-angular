import { Injectable, Inject } from '@angular/core';
import {Observable} from 'rxjs';
import { IUsuario } from '../../models/usuario/usuario.model';
import { ILogin } from '../../models/login/login.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class AutenticationService {

    constructor(private http:HttpClient, private router:Router, private cookieService: CookieService){ }


    private apiUrl = environment.BASE_API_URL

    login(login: ILogin){
        console.log(this.apiUrl+'login', login);
        return this.http.post(this.apiUrl+'login', login);
    }


    obtenerToken():string{
        return this.cookieService.get("token");
    }


    obtenerExpiracionToken():string{
        return this.cookieService.get("tokenExpiration");

    }

    logout(){
        this.cookieService.deleteAll();
        localStorage.removeItem("tokenExpiration");
        localStorage.removeItem("usuario_nombre");
        localStorage.removeItem("usuario_apellido");
        localStorage.removeItem("usuario_codigo");
        localStorage.removeItem("usuario_correo");
        this.router.navigate(['/login']);
    }

    estaLogueado():boolean{
        var exp = this.obtenerExpiracionToken();
        if(!exp){
            return false;
        }
        var now = new Date();
        var dateExp = new Date(exp);
        
        


        if (now.getTime() >= dateExp.getTime()) {
            // ya expiró el token
            this.cookieService.delete('token');
            this.cookieService.delete('tokenExpiration');
            return false;
        } else {
            
            return true;
            
        }
    }
}
