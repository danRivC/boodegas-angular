import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  constructor(private http:HttpClient) { }
  private apiUrl = environment.BASE_API_URL
  obtenerDetalle(bodega:number){
    return this.http.get( `${this.apiUrl}stock/${bodega}` );
  }
}
