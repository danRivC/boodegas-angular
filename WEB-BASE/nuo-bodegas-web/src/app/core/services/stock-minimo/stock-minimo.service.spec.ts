import { TestBed } from '@angular/core/testing';

import { StockMinimoService } from './stock-minimo.service';

describe('StockMinimoService', () => {
  let service: StockMinimoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StockMinimoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
