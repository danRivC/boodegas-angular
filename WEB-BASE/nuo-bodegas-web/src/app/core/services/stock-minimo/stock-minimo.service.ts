import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StockMinimoService {

  constructor(private http:HttpClient) { }
  private apiUrl = environment.BASE_API_URL
  obtenerDetalle(bodega:number){
    return this.http.get( `${this.apiUrl}stock/minimo/${bodega}` );
  }
}
