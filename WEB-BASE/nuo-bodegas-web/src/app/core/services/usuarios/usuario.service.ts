import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { IUsuario } from '../../models/usuario/usuario.model';
import { PerfilUsuario } from '../../models/perfil/perfil-usuario.model';
import * as Sentry from "@sentry/browser";

Sentry.init({
  dsn: "https://c3f88586f98b47b192c3c52bfa52be8c@sentry.io/5185655"
});


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http:HttpClient, private router:Router) { }
  private apiUrl = environment.BASE_API_URL;

  public obtenerTodosUsuarios(username:string, estado: boolean){
    return this.http.get(this.apiUrl+'usuario/'+estado+'/'+username);
  }

  public guardarUsuario(usuario: IUsuario){
    return this.http.post<IUsuario>(this.apiUrl+"usuario", usuario);
  }
  public obtenerUnUsuario(id:number){
    return this.http.get(this.apiUrl+'usuario/buscar/'+id);
  }
  public obtenerPerfilesUsuario(id: number){
    return this.http.get(this.apiUrl+'usuario/perfiles/'+id);
  }
  public insertarActualizarPerfilUsuario(perfil:PerfilUsuario){
    return this.http.post<PerfilUsuario>(this.apiUrl+'usuario/perfiles', perfil)
  }
  


}
