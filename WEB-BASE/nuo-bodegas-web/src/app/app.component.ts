import { Component, HostListener } from '@angular/core';
import { LoadingScreenService } from './shared/components/loading-screen/service/loading-screen.service';
import {SwUpdate} from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'nuo-bodegas-web';
  showLoader: boolean;
  constructor(
    private loaderService: LoadingScreenService,
    private swUpdate: SwUpdate
    ) {}
    updatePwa(){
      this.swUpdate.available.subscribe(value=>console.log(value));
      window.location.reload();
    }


ngOnInit() {
    this.loaderService.status.subscribe((val: boolean) => {
        this.showLoader = val;
    });
    this.updatePwa;
}
}
