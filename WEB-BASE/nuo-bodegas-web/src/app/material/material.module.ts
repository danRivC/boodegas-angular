import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatDatepickerModule} from '@angular/material/datepicker' ;
import {MatStepperModule} from '@angular/material/stepper';
import {MatListModule} from '@angular/material/list';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {CdkTableModule} from '@angular/cdk/table';





@NgModule({
  declarations: [],

  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSnackBarModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    CdkTableModule,
    MatBottomSheetModule,
    MatListModule,
  
  ],
  exports:[
    MatTableModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSnackBarModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    CdkTableModule,
    MatBottomSheetModule,
    MatListModule

  ]
})
export class MaterialModule { }
