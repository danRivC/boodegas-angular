import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AuthGuardService } from './core/auth-guard.services';
import { AuthInterceptorService } from './core/auth-interceptor.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';

//I keep the new line

//I keep the new line
import { AutenticationService } from './core/services/login/autentication.service';
import { LoadingScreenService } from './shared/components/loading-screen/service/loading-screen.service';

import { BodegaModule } from './bodega/bodega.module';
import { BaseModule } from './base/base.module';
import { CiudadModule } from './core/models/bodega/ciudad.module';
import { KardexModule } from './kardex/kardex.module';
import { LoginModule } from './login/login.module';
import { SharedModule } from './shared/shared.module';
import { ModelosModule } from './modelos/modelos.module';
import { PerfilModule } from './perfil/perfil.module';
import { ProductoModule } from './producto/producto.module';
import { ProveedorModule } from './proveedor/proveedor.module';
import { SalidaModule } from './salida/salida.module';
import { UsuarioModule } from './usuario/usuario.module';
import { UbicacionModule } from './ubicacion/ubicacion.module';
import { TipoProductoModule } from './tipo-producto/tipo-producto.module';
import { StockModule } from './stock/stock.module';
import { LoadingPantallaComponent } from './loading-pantalla/components/loading-pantalla.component';
import { ProductoModuleUsuario } from './producto-usuario/producto.module';
import { StockMinimoModule } from './stock-minimo/stock-minimo.module';
import { CoreModule } from './core/core.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { IgxBannerModule, IgxAvatarModule, IgxRippleModule, IgxIconModule } from 'igniteui-angular';

//I keep the new line
@NgModule({
  declarations: [
    AppComponent,
    LoadingPantallaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    BodegaModule,
    BaseModule,
    CiudadModule,
    KardexModule,
    LoginModule,
    SharedModule,
    ModelosModule,
    PerfilModule,
    ProductoModule,
    ProveedorModule,
    SalidaModule,
    UsuarioModule,
    UbicacionModule,
    TipoProductoModule,
    StockModule,
    ProductoModuleUsuario,
    StockMinimoModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    
  ],
  entryComponents: [],
  providers: [
    AuthGuardService,
    AuthInterceptorService,
    AutenticationService,
    LoadingScreenService,
    CoreModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
