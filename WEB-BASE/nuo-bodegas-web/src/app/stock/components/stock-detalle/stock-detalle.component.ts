import { Component, OnInit, AfterViewInit ,ViewChild } from '@angular/core';
import { StockService } from '../../../core/services/stock/stock.service';
import {Location} from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ExporterService } from 'src/app/core/exporter.service';
import { LoadingPantallaService } from 'src/app/loading-pantalla/services/loading-pantalla.service';
import { timestamp } from 'rxjs/operators';
@Component({
  selector: 'app-stock-detalle',
  templateUrl: './stock-detalle.component.html',
  styleUrls: ['./stock-detalle.component.scss']
})
export class StockDetalleComponent implements OnInit {

  titulo = 'Detalle de Stock'
  constructor(private stockService: StockService ,private snackBar:MatSnackBar,private location: Location, 
    private exporterService:ExporterService, private loadingPantallaService:LoadingPantallaService) { }
  dataSource = new MatTableDataSource<IStock>();
  @ViewChild(MatPaginator, {static:true}) paginador: MatPaginator;
  displayColumns: string[] = ['parte', 'nombre', 'serie','cantidad',  'modelo', 'ubicacion'];
  productos : Array<IStock>;
  busquedaForm : FormGroup;
  
  
  ngOnInit() {
    
    let codigo_bodega = parseInt(sessionStorage.getItem('codigoBodega'))
   this.stockService.obtenerDetalle(codigo_bodega).subscribe(datos=>this.asignarProductos(datos), error=>console.log(error))
   this.dataSource.paginator = this.paginador;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  

  asignarProductos(datos){
    console.log(datos)
    let producto:IStock;
    this.productos = []
    datos.resultado.forEach(element => {
       producto= new IStock();
  
      producto.nombre = element.nombre;
      producto.serie = element.serie;
      producto.parte = element.parte;
      producto.cantidad = element.cantidad;
    
      producto.modelo = element.modelo;
      producto.ubicacion = element.ubicacion;

      this.productos.push(producto);
    });
    this.dataSource.data = this.productos;
  }
  regresar(){
    this.location.back();
    sessionStorage.clear();
  }

  exporterAsExcel(){
   
    this.exporterService.exportToExcel(this.dataSource.filteredData, 'stock_reporte');
    
    
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
class IStock{
  parte: string;
  nombre: string;
  serie : string;
  cantidad :number;
  modelo:string;
  ubicacion :string;
}

