import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { StockComponent } from './components/stock/stock.component';
import { StockDetalleComponent } from './components/stock-detalle/stock-detalle.component';

const routes : Routes = [
  {path:'', component:StockComponent},
  {path:'detalle', component:StockDetalleComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class StockRoutingModule { }
