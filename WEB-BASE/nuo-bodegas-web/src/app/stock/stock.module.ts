import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockComponent } from './components/stock/stock.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../material/material.module';
import { StockRoutingModule } from './stock-routing.module';
import { StockDetalleComponent } from './components/stock-detalle/stock-detalle.component';



@NgModule({
  declarations: [
    StockComponent,
    StockDetalleComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    StockRoutingModule
  ]
})
export class StockModule { }
