import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import { Router } from '@angular/router';
import { IgxBannerComponent } from "igniteui-angular";

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {

  constructor(private router: Router, ) { 
    this.router.navigate(['/home']);
  }
  @ViewChild(IgxBannerComponent, { static: true }) public banner: IgxBannerComponent;
  public contentWidth = "384px";
  
  installEvent = null;
@HostListener('window:beforeinstallprompt', ['$event'])
onBeforeInstallPrompt(event:Event){
  console.log(event);
  event.preventDefault();
  this.installEvent = event;
  this.banner.open();
}
installUsuario(){
  if(this.installEvent){
    this.installEvent.prompt();
    this.installEvent.userChoice
    .then(rta=>{
      console.log(rta)
    });
    this.banner.close();
  }
}

  ngOnInit() {
    this.router.navigate(['/home']);
  }

}
