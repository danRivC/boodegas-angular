import { Component, OnInit } from '@angular/core';
import { PaginasService } from '../../../core/services/pagina/paginas.service';
import { IPagina } from '../../../core/models/pagina/pagina.model';



@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  
  constructor(private paginasService:PaginasService) { }
  private codigouser= localStorage.getItem("usuario_codigo");
  paginas;
  grupoPaginas:GrupoPaginas[] ;
  contadorPaginas: GrupoPaginaContador[];
  ngOnInit() {
    this.paginasService.obtenerPaginas(parseInt(this.codigouser), 'true' ).subscribe(paginas=>this.mapearGrupoContador(paginas), error=>this.manejarError(error));
  }
  obtenerPaginas(pagina){
    this.paginas = pagina.resultado;
  }
  mapearGrupoContador(pagina){
    console.log(pagina)
    let grupo : GrupoPaginas;
    let paginas : IPagina;
    let grupoPaginasRepetidas : GrupoPaginas[];
    grupoPaginasRepetidas = [];
    pagina.resultado.forEach(element => {
      grupo = new GrupoPaginas();
      grupo.nombre = element.grupo;
      grupo.icono = element.iconoGrupo;
      grupoPaginasRepetidas.push(grupo);
    });
    console.log(this.grupoPaginas);
    this.grupoPaginas = grupoPaginasRepetidas.filter((valorActual, indiceActual, array)=>{
      return array.findIndex(
        valorDeArreglo => JSON.stringify(valorDeArreglo) === JSON.stringify(valorActual))===indiceActual
    })
    grupoPaginasRepetidas.forEach(paginaGrupo => {
      paginaGrupo.pagina = []
      pagina.resultado.forEach(element => {
        if(element.grupo === paginaGrupo.nombre){
          paginas = new IPagina();
          paginas.codigoPagina = element.codigoPagina;
          paginas.nombrePagina = element.nombrePagina;
          paginas.graficoPagina = element.graficoPagina;
          paginas.linkPagina = element.linkPagina;
          paginaGrupo.pagina.push(paginas)
        }
      });
    });
    



    console.log(this.grupoPaginas)
    
  }
  manejarError(error) {
    alert(error.mensaje);
    console.log(error);
  }

}
class GrupoPaginaContador{
  nombre: string;
  veces: number;
}
class GrupoPaginas{
  nombre: string;
  icono: string;
  pagina: Array<IPagina>;
}
