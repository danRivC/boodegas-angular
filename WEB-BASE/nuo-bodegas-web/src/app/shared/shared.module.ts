import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TituloComponent } from './components/titulo/titulo.component';
import { LoadingScreenComponent } from './components/loading-screen/loading-screen.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PipeBusquedaFechaPipe } from './pipes/pipe-busqueda-fecha.pipe';
import { IgxBannerModule, IgxAvatarModule, IgxRippleModule, IgxIconModule, IgxSwitchModule, IgxInputGroupModule, IgxButtonModule, IgxNavbarModule } from 'igniteui-angular';



@NgModule({
  declarations: [
    TituloComponent,
    LoadingScreenComponent,
    PipeBusquedaFechaPipe,
    
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IgxBannerModule,
		IgxAvatarModule,
		IgxRippleModule,
    IgxIconModule,
    IgxInputGroupModule,
		IgxButtonModule,
		IgxNavbarModule,
		IgxSwitchModule
  ],
  exports:[
    TituloComponent,
    LoadingScreenComponent,
    ReactiveFormsModule,
    FormsModule,
    IgxBannerModule,
		IgxAvatarModule,
		IgxRippleModule,
		IgxIconModule,
		IgxInputGroupModule,
		IgxButtonModule,
		IgxNavbarModule,
    IgxSwitchModule
  ]
})
export class SharedModule { }
