﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nuo_api_bodegas.Models
{
    public class Stock
    {
        public string parte { get; set; }
        public string nombre { get; set; }
        public string serie { get; set; }
        public int cantidad { get; set; }
        public string ubicacion { get; set; }
        public string modelo { get; set; }
    }
}
