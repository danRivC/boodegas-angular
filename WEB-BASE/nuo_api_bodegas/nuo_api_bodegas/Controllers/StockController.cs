﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using nuo_api_bodegas.Data;
using nuo_api_bodegas.Models;

namespace nuo_api_bodegas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StockController : ControllerBase
    {
        private readonly ValoresStock _stock;
        public StockController(ValoresStock stock)
        {
            _stock = stock ?? throw new ArgumentNullException(nameof(stock));
        }

        [HttpGet("{bodega}")]
        public async Task<ActionResult<BaseResponse>> DevuelveProductos(int bodega)
        {
            try
            {
                return await _stock.DevuelveStockBodegas(bodega);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpGet("minimo/{bodega}")]
        public async Task<ActionResult<BaseResponse>> StockMinimo(int bodega)
        {
            try
            {
                return await _stock.DevuelveStockMinimo(bodega);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}