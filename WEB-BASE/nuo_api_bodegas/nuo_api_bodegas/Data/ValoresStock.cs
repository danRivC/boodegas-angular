﻿
using Microsoft.Extensions.Configuration;
using nuo_api_bodegas.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace nuo_api_bodegas.Data
{
    public class ValoresStock
    {


        private readonly string _connectionString;
        private readonly IConfiguration _configuration;
        public ValoresStock(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }


        public async Task<BaseResponse> DevuelveStockMinimo(int id)
        {
            using (SqlConnection sql = new SqlConnection(_connectionString))
            {
                BaseResponse response = new BaseResponse();
                response.resultado = new List<object>();
                int codigoMovimiento = 0;
                try
                {
                    using (SqlCommand cmd = new SqlCommand("SP_DEVUELVE_STOCK_MINIMO_BODEGA", sql))
                    {
                        cmd.Parameters.Add(new SqlParameter("@CODIGO_BOD", id));

                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        response.status = "correcto";
                        response.mensaje = "consulta Correcta";
                        Stock stock = new Stock();
                        response.codigo = "201";
                        await sql.OpenAsync();
                        using (var reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                stock = MapearStock(reader);
                                response.resultado.Add(stock);
                            }
                        }
                    }
                    return response;

                }
                catch (Exception er)
                {
                    response.status = "correcto";
                    response.mensaje = "No se pudo guardar la entrada " + er.Message;
                    response.codigo = "410";
                    return response;
                }
            }
        }
    

        public async Task<BaseResponse> DevuelveStockBodegas(int bodega)
        {
            using (SqlConnection sql = new SqlConnection(_connectionString))
            {
                BaseResponse response = new BaseResponse();
                response.resultado = new List<object>();
                int codigoMovimiento = 0;
                try
                {
                    using (SqlCommand cmd = new SqlCommand("SP_DEVUELVE_STOCK_BODEGA", sql))
                    {
                        cmd.Parameters.Add(new SqlParameter("@CODIGO_BOD", bodega));
                        
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        response.status = "correcto";
                        response.mensaje = "Ciudad Guardada Correctamente";
                        Stock stock = new Stock();
                        response.codigo = "201";
                        await sql.OpenAsync();
                        using (var reader = await cmd.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                stock = MapearStock(reader);
                                response.resultado.Add(stock);
                            }
                        }
                    }
                    return response;

                }
                catch (Exception er)
                {
                    response.status = "correcto";
                    response.mensaje = "No se pudo guardar la entrada " + er.Message;
                    response.codigo = "410";
                    return response;
                }
            }
        }
        public Stock MapearStock(SqlDataReader reader)
        {
            Stock stock = new Stock();
            stock.parte = Convert.ToString(reader["NUM_PART_PRO"]);
            stock.nombre = Convert.ToString(reader["NOMBRE_PRO"]);
            stock.serie = Convert.ToString(reader["NUM_SERIE_PRO"]);
            stock.cantidad = Convert.ToInt32(reader["CANTIDAD_INV"]);
            stock.ubicacion = Convert.ToString(reader["DESCRIPCION_UBI"]);
            stock.modelo = Convert.ToString(reader["NOMBRE_MODELO"]);
            return stock;
        }
    }
}
